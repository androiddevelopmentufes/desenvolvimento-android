package com.example.ricardo.trabalho_v3;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

public class Anuncio extends AppCompatActivity {

    private Spinner nDisciplinas;
    private Spinner nNivelAulas;

    Button postarAnuncio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anuncio);

        nDisciplinas = (Spinner)findViewById(R.id.sp_disciplinaAnuncio);
        ArrayAdapter adapter2 = ArrayAdapter.createFromResource(this, R.array.disciplinas, android.R.layout.simple_spinner_item);
        nDisciplinas.setAdapter(adapter2);

        nNivelAulas = (Spinner)findViewById(R.id.sp_nivelAnuncio);
        ArrayAdapter adapter3 = ArrayAdapter.createFromResource(this, R.array.nivelAula, android.R.layout.simple_spinner_item);
        nNivelAulas.setAdapter(adapter3);

        postarAnuncio = (Button)findViewById(R.id.bt_postarAnuncio);
        postarAnuncio.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v)
            {
                Intent acessarServicos = new Intent(Anuncio.this,Servicos.class);
                startActivity(acessarServicos);
            }
        });
    }
}
